#locker

[![Build Status](https://api.travis-ci.org/pandorajs/locker.png?branch=master)](http://travis-ci.org/pandorajs/locker)
[![Coverage Status](https://coveralls.io/repos/pandorajs/locker/badge.png?branch=master)](https://coveralls.io/r/pandorajs/locker?branch=master)

 > locker, seajs module

##how to demo

1. checkout
1. run `npm install`
1. run `grunt`
1. view files in `/demo`

##how to use

1. run `spm install pandora/locker`
1. write `require('pandora/locker/VERSION.NUMBER/locker')`

##find examples

1. view the source files in '/src'

##history

- 1.0.0 - release
